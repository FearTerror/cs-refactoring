-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 20 2023 г., 03:04
-- Версия сервера: 10.6.12-MariaDB-0ubuntu0.22.04.1
-- Версия PHP: 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `distribution`
--
CREATE DATABASE IF NOT EXISTS `distribution` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `distribution`;

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_product` (IN `product_name` VARCHAR(255), IN `product_quantity_per_unit` VARCHAR(255), IN `product_unit_price` DECIMAL(10,4), IN `supplier_company_name` VARCHAR(255), IN `supplier_address` VARCHAR(255), IN `supplier_city` VARCHAR(255), IN `supplier_country` VARCHAR(255), IN `zip` VARCHAR(255), IN `supplier_contact_name` VARCHAR(255), IN `supplier_contact_title` VARCHAR(255))   BEGIN

INSERT INTO products(product, price, supplier, manager)
VALUES ( concat_ws(' | ', product_name, product_quantity_per_unit)
    , product_unit_price
    , json_object('company', supplier_company_name, 'address', supplier_address, 'city', supplier_city, 'country', supplier_country, 'zip', zip)
    , concat_ws(' | ', supplier_contact_name, supplier_contact_title));

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,4) DEFAULT 0.0000,
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manager` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`product`, `price`, `supplier`, `manager`) VALUES
('Chai | 10 boxes x 20 bags', 18.0000, '{\"company\": \"Exotic Liquids\", \"address\": \"49 Gilbert St.\", \"city\": \"London\", \"country\": \"UK\", \"zip\": \"EC1 4SD\"}', 'Charlotte Cooper | Purchasing Manager'),
('Chang | 24 - 12 oz bottles', 19.0000, '{\"company\": \"Exotic Liquids\", \"address\": \"49 Gilbert St.\", \"city\": \"London\", \"country\": \"UK\", \"zip\": \"EC1 4SD\"}', 'Charlotte Cooper | Purchasing Manager'),
('Aniseed Syrup | 12 - 550 ml bottles', 10.0000, '{\"company\": \"Exotic Liquids\", \"address\": \"49 Gilbert St.\", \"city\": \"London\", \"country\": \"UK\", \"zip\": \"EC1 4SD\"}', 'Charlotte Cooper | Purchasing Manager'),
('Chef Anton\'s Cajun Seasoning | 48 - 6 oz jars', 22.0000, '{\"company\": \"New Orleans Cajun Delights\", \"address\": \"P.O. Box 78934\", \"city\": \"New Orleans\", \"country\": \"USA\", \"zip\": \"70117\"}', 'Shelley Burke | Order Administrator'),
('Chef Anton\'s Gumbo Mix | 36 boxes', 21.3500, '{\"company\": \"New Orleans Cajun Delights\", \"address\": \"P.O. Box 78934\", \"city\": \"New Orleans\", \"country\": \"USA\", \"zip\": \"70117\"}', 'Shelley Burke | Order Administrator'),
('Louisiana Fiery Hot Pepper Sauce | 32 - 8 oz bottles', 21.0500, '{\"company\": \"New Orleans Cajun Delights\", \"address\": \"P.O. Box 78934\", \"city\": \"New Orleans\", \"country\": \"USA\", \"zip\": \"70117\"}', 'Shelley Burke | Order Administrator'),
('Louisiana Hot Spiced Okra | 24 - 8 oz jars', 17.0000, '{\"company\": \"New Orleans Cajun Delights\", \"address\": \"P.O. Box 78934\", \"city\": \"New Orleans\", \"country\": \"USA\", \"zip\": \"70117\"}', 'Shelley Burke | Order Administrator'),
('Grandma\'s Boysenberry Spread | 12 - 8 oz jars', 25.0000, '{\"company\": \"Grandma Kelly\'s Homestead\", \"address\": \"707 Oxford Rd.\", \"city\": \"Ann Arbor\", \"country\": \"USA\", \"zip\": \"48104\"}', 'Regina Murphy | Sales Representative'),
('Uncle Bob\'s Organic Dried Pears | 12 - 1 lb pkgs.', 30.0000, '{\"company\": \"Grandma Kelly\'s Homestead\", \"address\": \"707 Oxford Rd.\", \"city\": \"Ann Arbor\", \"country\": \"USA\", \"zip\": \"48104\"}', 'Regina Murphy | Sales Representative'),
('Northwoods Cranberry Sauce | 12 - 12 oz jars', 40.0000, '{\"company\": \"Grandma Kelly\'s Homestead\", \"address\": \"707 Oxford Rd.\", \"city\": \"Ann Arbor\", \"country\": \"USA\", \"zip\": \"48104\"}', 'Regina Murphy | Sales Representative'),
('Mishi Kobe Niku | 18 - 500 g pkgs.', 97.0000, '{\"company\": \"Tokyo Traders\", \"address\": \"9-8 Sekimai\\r\\nMusashino-shi\", \"city\": \"Tokyo\", \"country\": \"Japan\", \"zip\": \"100\"}', 'Yoshi Nagase | Marketing Manager'),
('Ikura | 12 - 200 ml jars', 31.0000, '{\"company\": \"Tokyo Traders\", \"address\": \"9-8 Sekimai\\r\\nMusashino-shi\", \"city\": \"Tokyo\", \"country\": \"Japan\", \"zip\": \"100\"}', 'Yoshi Nagase | Marketing Manager'),
('Longlife Tofu | 5 kg pkg.', 10.0000, '{\"company\": \"Tokyo Traders\", \"address\": \"9-8 Sekimai\\r\\nMusashino-shi\", \"city\": \"Tokyo\", \"country\": \"Japan\", \"zip\": \"100\"}', 'Yoshi Nagase | Marketing Manager'),
('Queso Cabrales | 1 kg pkg.', 21.0000, '{\"company\": \"Cooperativa de Quesos \'Las Cabras\'\", \"address\": \"Calle del Rosal 4\", \"city\": \"Oviedo\", \"country\": \"Spain\", \"zip\": \"33007\"}', 'Antonio del Valle Saavedra  | Export Administrator'),
('Queso Manchego La Pastora | 10 - 500 g pkgs.', 38.0000, '{\"company\": \"Cooperativa de Quesos \'Las Cabras\'\", \"address\": \"Calle del Rosal 4\", \"city\": \"Oviedo\", \"country\": \"Spain\", \"zip\": \"33007\"}', 'Antonio del Valle Saavedra  | Export Administrator'),
('Konbu | 2 kg box', 6.0000, '{\"company\": \"Mayumi\'s\", \"address\": \"92 Setsuko\\r\\nChuo-ku\", \"city\": \"Osaka\", \"country\": \"Japan\", \"zip\": \"545\"}', 'Mayumi Ohno | Marketing Representative'),
('Tofu | 40 - 100 g pkgs.', 23.2500, '{\"company\": \"Mayumi\'s\", \"address\": \"92 Setsuko\\r\\nChuo-ku\", \"city\": \"Osaka\", \"country\": \"Japan\", \"zip\": \"545\"}', 'Mayumi Ohno | Marketing Representative'),
('Genen Shouyu | 24 - 250 ml bottles', 15.5000, '{\"company\": \"Mayumi\'s\", \"address\": \"92 Setsuko\\r\\nChuo-ku\", \"city\": \"Osaka\", \"country\": \"Japan\", \"zip\": \"545\"}', 'Mayumi Ohno | Marketing Representative'),
('Pavlova | 32 - 500 g boxes', 17.4500, '{\"company\": \"Pavlova, Ltd.\", \"address\": \"74 Rose St.\\r\\nMoonie Ponds\", \"city\": \"Melbourne\", \"country\": \"Australia\", \"zip\": \"3058\"}', 'Ian Devling | Marketing Manager'),
('Alice Mutton | 20 - 1 kg tins', 39.0000, '{\"company\": \"Pavlova, Ltd.\", \"address\": \"74 Rose St.\\r\\nMoonie Ponds\", \"city\": \"Melbourne\", \"country\": \"Australia\", \"zip\": \"3058\"}', 'Ian Devling | Marketing Manager'),
('Carnarvon Tigers | 16 kg pkg.', 62.5000, '{\"company\": \"Pavlova, Ltd.\", \"address\": \"74 Rose St.\\r\\nMoonie Ponds\", \"city\": \"Melbourne\", \"country\": \"Australia\", \"zip\": \"3058\"}', 'Ian Devling | Marketing Manager'),
('Vegie-spread | 15 - 625 g jars', 43.9000, '{\"company\": \"Pavlova, Ltd.\", \"address\": \"74 Rose St.\\r\\nMoonie Ponds\", \"city\": \"Melbourne\", \"country\": \"Australia\", \"zip\": \"3058\"}', 'Ian Devling | Marketing Manager'),
('Outback Lager | 24 - 355 ml bottles', 15.0000, '{\"company\": \"Pavlova, Ltd.\", \"address\": \"74 Rose St.\\r\\nMoonie Ponds\", \"city\": \"Melbourne\", \"country\": \"Australia\", \"zip\": \"3058\"}', 'Ian Devling | Marketing Manager'),
('Teatime Chocolate Biscuits | 10 boxes x 12 pieces', 9.2000, '{\"company\": \"Specialty Biscuits, Ltd.\", \"address\": \"29 King\'s Way\", \"city\": \"Manchester\", \"country\": \"UK\", \"zip\": \"M14 GSD\"}', 'Peter Wilson | Sales Representative'),
('Sir Rodney\'s Marmalade | 30 gift boxes', 81.0000, '{\"company\": \"Specialty Biscuits, Ltd.\", \"address\": \"29 King\'s Way\", \"city\": \"Manchester\", \"country\": \"UK\", \"zip\": \"M14 GSD\"}', 'Peter Wilson | Sales Representative'),
('Sir Rodney\'s Scones | 24 pkgs. x 4 pieces', 10.0000, '{\"company\": \"Specialty Biscuits, Ltd.\", \"address\": \"29 King\'s Way\", \"city\": \"Manchester\", \"country\": \"UK\", \"zip\": \"M14 GSD\"}', 'Peter Wilson | Sales Representative'),
('Scottish Longbreads | 10 boxes x 8 pieces', 12.5000, '{\"company\": \"Specialty Biscuits, Ltd.\", \"address\": \"29 King\'s Way\", \"city\": \"Manchester\", \"country\": \"UK\", \"zip\": \"M14 GSD\"}', 'Peter Wilson | Sales Representative'),
('Gustaf\'s Knckebrd | 24 - 500 g pkgs.', 21.0000, '{\"company\": \"PB Knckebrd AB\", \"address\": \"Kaloadagatan 13\", \"city\": \"Gteborg\", \"country\": \"Sweden \", \"zip\": \"S-345 67\"}', 'Lars Peterson | Sales Agent'),
('Tunnbrd | 12 - 250 g pkgs.', 9.0000, '{\"company\": \"PB Knckebrd AB\", \"address\": \"Kaloadagatan 13\", \"city\": \"Gteborg\", \"country\": \"Sweden \", \"zip\": \"S-345 67\"}', 'Lars Peterson | Sales Agent'),
('Guaran Fantstica | 12 - 355 ml cans', 4.5000, '{\"company\": \"Refrescos Americanas LTDA\", \"address\": \"Av. das Americanas 12.890\", \"city\": \"So Paulo\", \"country\": \"Brazil\", \"zip\": \"5442\"}', 'Carlos Diaz | Marketing Manager'),
('NuNuCa Nu-Nougat-Creme | 20 - 450 g glasses', 14.0000, '{\"company\": \"Heli Swaren GmbH & Co. KG\", \"address\": \"Tiergartenstrae 5\", \"city\": \"Berlin\", \"country\": \"Germany\", \"zip\": \"10785\"}', 'Petra Winkler | Sales Manager'),
('Gumbr Gummibrchen | 100 - 250 g bags', 31.2300, '{\"company\": \"Heli Swaren GmbH & Co. KG\", \"address\": \"Tiergartenstrae 5\", \"city\": \"Berlin\", \"country\": \"Germany\", \"zip\": \"10785\"}', 'Petra Winkler | Sales Manager'),
('Schoggi Schokolade | 100 - 100 g pieces', 43.9000, '{\"company\": \"Heli Swaren GmbH & Co. KG\", \"address\": \"Tiergartenstrae 5\", \"city\": \"Berlin\", \"country\": \"Germany\", \"zip\": \"10785\"}', 'Petra Winkler | Sales Manager'),
('Rssle Sauerkraut | 25 - 825 g cans', 45.6000, '{\"company\": \"Plutzer Lebensmittelgromrkte AG\", \"address\": \"Bogenallee 51\", \"city\": \"Frankfurt\", \"country\": \"Germany\", \"zip\": \"60439\"}', 'Martin Bein | International Marketing Mgr.'),
('Thringer Rostbratwurst | 50 bags x 30 sausgs.', 123.7900, '{\"company\": \"Plutzer Lebensmittelgromrkte AG\", \"address\": \"Bogenallee 51\", \"city\": \"Frankfurt\", \"country\": \"Germany\", \"zip\": \"60439\"}', 'Martin Bein | International Marketing Mgr.'),
('Wimmers gute Semmelkndel | 20 bags x 4 pieces', 33.2500, '{\"company\": \"Plutzer Lebensmittelgromrkte AG\", \"address\": \"Bogenallee 51\", \"city\": \"Frankfurt\", \"country\": \"Germany\", \"zip\": \"60439\"}', 'Martin Bein | International Marketing Mgr.'),
('Rhnbru Klosterbier | 24 - 0.5 l bottles', 7.7500, '{\"company\": \"Plutzer Lebensmittelgromrkte AG\", \"address\": \"Bogenallee 51\", \"city\": \"Frankfurt\", \"country\": \"Germany\", \"zip\": \"60439\"}', 'Martin Bein | International Marketing Mgr.'),
('Original Frankfurter grne Soe | 12 boxes', 13.0000, '{\"company\": \"Plutzer Lebensmittelgromrkte AG\", \"address\": \"Bogenallee 51\", \"city\": \"Frankfurt\", \"country\": \"Germany\", \"zip\": \"60439\"}', 'Martin Bein | International Marketing Mgr.'),
('Nord-Ost Matjeshering | 10 - 200 g glasses', 25.8900, '{\"company\": \"Nord-Ost-Fisch Handelsgesellschaft mbH\", \"address\": \"Frahmredder 112a\", \"city\": \"Cuxhaven\", \"country\": \"Germany\", \"zip\": \"27478\"}', 'Sven Petersen | Coordinator Foreign Markets'),
('Gorgonzola Telino | 12 - 100 g pkgs', 12.5000, '{\"company\": \"Formaggi Fortini s.r.l.\", \"address\": \"Viale Dante, 75\", \"city\": \"Ravenna\", \"country\": \"Italy\", \"zip\": \"48100\"}', 'Elio Rossi | Sales Representative'),
('Mascarpone Fabioli | 24 - 200 g pkgs.', 32.0000, '{\"company\": \"Formaggi Fortini s.r.l.\", \"address\": \"Viale Dante, 75\", \"city\": \"Ravenna\", \"country\": \"Italy\", \"zip\": \"48100\"}', 'Elio Rossi | Sales Representative'),
('Mozzarella di Giovanni | 24 - 200 g pkgs.', 34.8000, '{\"company\": \"Formaggi Fortini s.r.l.\", \"address\": \"Viale Dante, 75\", \"city\": \"Ravenna\", \"country\": \"Italy\", \"zip\": \"48100\"}', 'Elio Rossi | Sales Representative'),
('Geitost | 500 g', 2.5000, '{\"company\": \"Norske Meierier\", \"address\": \"Hatlevegen 5\", \"city\": \"Sandvika\", \"country\": \"Norway\", \"zip\": \"1320\"}', 'Beate Vileid | Marketing Manager'),
('Gudbrandsdalsost | 10 kg pkg.', 36.0000, '{\"company\": \"Norske Meierier\", \"address\": \"Hatlevegen 5\", \"city\": \"Sandvika\", \"country\": \"Norway\", \"zip\": \"1320\"}', 'Beate Vileid | Marketing Manager'),
('Flotemysost | 10 - 500 g pkgs.', 21.5000, '{\"company\": \"Norske Meierier\", \"address\": \"Hatlevegen 5\", \"city\": \"Sandvika\", \"country\": \"Norway\", \"zip\": \"1320\"}', 'Beate Vileid | Marketing Manager'),
('Sasquatch Ale | 24 - 12 oz bottles', 14.0000, '{\"company\": \"Bigfoot Breweries\", \"address\": \"3400 - 8th Avenue\\r\\nSuite 210\", \"city\": \"Bend\", \"country\": \"USA\", \"zip\": \"97101\"}', 'Cheryl Saylor | Regional Account Rep.'),
('Steeleye Stout | 24 - 12 oz bottles', 18.0000, '{\"company\": \"Bigfoot Breweries\", \"address\": \"3400 - 8th Avenue\\r\\nSuite 210\", \"city\": \"Bend\", \"country\": \"USA\", \"zip\": \"97101\"}', 'Cheryl Saylor | Regional Account Rep.'),
('Laughing Lumberjack Lager | 24 - 12 oz bottles', 14.0000, '{\"company\": \"Bigfoot Breweries\", \"address\": \"3400 - 8th Avenue\\r\\nSuite 210\", \"city\": \"Bend\", \"country\": \"USA\", \"zip\": \"97101\"}', 'Cheryl Saylor | Regional Account Rep.'),
('Inlagd Sill | 24 - 250 g  jars', 19.0000, '{\"company\": \"Svensk Sjfda AB\", \"address\": \"Brovallavgen 231\", \"city\": \"Stockholm\", \"country\": \"Sweden\", \"zip\": \"S-123 45\"}', 'Michael Bjrn | Sales Representative'),
('Gravad lax | 12 - 500 g pkgs.', 26.0000, '{\"company\": \"Svensk Sjfda AB\", \"address\": \"Brovallavgen 231\", \"city\": \"Stockholm\", \"country\": \"Sweden\", \"zip\": \"S-123 45\"}', 'Michael Bjrn | Sales Representative'),
('Rd Kaviar | 24 - 150 g jars', 15.0000, '{\"company\": \"Svensk Sjfda AB\", \"address\": \"Brovallavgen 231\", \"city\": \"Stockholm\", \"country\": \"Sweden\", \"zip\": \"S-123 45\"}', 'Michael Bjrn | Sales Representative'),
('Cte de Blaye | 12 - 75 cl bottles', 263.5000, '{\"company\": \"Aux joyeux ecclsiastiques\", \"address\": \"203, Rue des Francs-Bourgeois\", \"city\": \"Paris\", \"country\": \"France\", \"zip\": \"75004\"}', 'Guylne Nodier | Sales Manager'),
('Chartreuse verte | 750 cc per bottle', 18.0000, '{\"company\": \"Aux joyeux ecclsiastiques\", \"address\": \"203, Rue des Francs-Bourgeois\", \"city\": \"Paris\", \"country\": \"France\", \"zip\": \"75004\"}', 'Guylne Nodier | Sales Manager'),
('Boston Crab Meat | 24 - 4 oz tins', 18.4000, '{\"company\": \"New England Seafood Cannery\", \"address\": \"Order Processing Dept.\\r\\n2100 Paul Revere Blvd.\", \"city\": \"Boston\", \"country\": \"USA\", \"zip\": \"02134\"}', 'Robb Merchant | Wholesale Account Agent'),
('Jack\'s New England Clam Chowder | 12 - 12 oz cans', 9.6500, '{\"company\": \"New England Seafood Cannery\", \"address\": \"Order Processing Dept.\\r\\n2100 Paul Revere Blvd.\", \"city\": \"Boston\", \"country\": \"USA\", \"zip\": \"02134\"}', 'Robb Merchant | Wholesale Account Agent'),
('Singaporean Hokkien Fried Mee | 32 - 1 kg pkgs.', 14.0000, '{\"company\": \"Leka Trading\", \"address\": \"471 Serangoon Loop, Suite #402\", \"city\": \"Singapore\", \"country\": \"Singapore\", \"zip\": \"0512\"}', 'Chandra Leka | Owner'),
('Ipoh Coffee | 16 - 500 g tins', 46.0000, '{\"company\": \"Leka Trading\", \"address\": \"471 Serangoon Loop, Suite #402\", \"city\": \"Singapore\", \"country\": \"Singapore\", \"zip\": \"0512\"}', 'Chandra Leka | Owner'),
('Gula Malacca | 20 - 2 kg bags', 19.4500, '{\"company\": \"Leka Trading\", \"address\": \"471 Serangoon Loop, Suite #402\", \"city\": \"Singapore\", \"country\": \"Singapore\", \"zip\": \"0512\"}', 'Chandra Leka | Owner'),
('Rogede sild | 1k pkg.', 9.5000, '{\"company\": \"Lyngbysild\", \"address\": \"Lyngbysild\\r\\nFiskebakken 10\", \"city\": \"Lyngby\", \"country\": \"Denmark\", \"zip\": \"2800\"}', 'Niels Petersen | Sales Manager'),
('Spegesild | 4 - 450 g glasses', 12.0000, '{\"company\": \"Lyngbysild\", \"address\": \"Lyngbysild\\r\\nFiskebakken 10\", \"city\": \"Lyngby\", \"country\": \"Denmark\", \"zip\": \"2800\"}', 'Niels Petersen | Sales Manager'),
('Zaanse koeken | 10 - 4 oz boxes', 9.5000, '{\"company\": \"Zaanse Snoepfabriek\", \"address\": \"Verkoop\\r\\nRijnweg 22\", \"city\": \"Zaandam\", \"country\": \"Netherlands\", \"zip\": \"9999 ZZ\"}', 'Dirk Luchte | Accounting Manager'),
('Chocolade | 10 pkgs.', 12.7500, '{\"company\": \"Zaanse Snoepfabriek\", \"address\": \"Verkoop\\r\\nRijnweg 22\", \"city\": \"Zaandam\", \"country\": \"Netherlands\", \"zip\": \"9999 ZZ\"}', 'Dirk Luchte | Accounting Manager'),
('Maxilaku | 24 - 50 g pkgs.', 20.0000, '{\"company\": \"Karkki Oy\", \"address\": \"Valtakatu 12\", \"city\": \"Lappeenranta\", \"country\": \"Finland\", \"zip\": \"53120\"}', 'Anne Heikkonen | Product Manager'),
('Valkoinen suklaa | 12 - 100 g bars', 16.2500, '{\"company\": \"Karkki Oy\", \"address\": \"Valtakatu 12\", \"city\": \"Lappeenranta\", \"country\": \"Finland\", \"zip\": \"53120\"}', 'Anne Heikkonen | Product Manager'),
('Lakkalikri | 500 ml', 18.0000, '{\"company\": \"Karkki Oy\", \"address\": \"Valtakatu 12\", \"city\": \"Lappeenranta\", \"country\": \"Finland\", \"zip\": \"53120\"}', 'Anne Heikkonen | Product Manager'),
('Manjimup Dried Apples | 50 - 300 g pkgs.', 53.0000, '{\"company\": \"G\'day, Mate\", \"address\": \"170 Prince Edward Parade\\r\\nHunter\'s Hill\", \"city\": \"Sydney\", \"country\": \"Australia\", \"zip\": \"2042\"}', 'Wendy Mackenzie | Sales Representative'),
('Filo Mix | 16 - 2 kg boxes', 7.0000, '{\"company\": \"G\'day, Mate\", \"address\": \"170 Prince Edward Parade\\r\\nHunter\'s Hill\", \"city\": \"Sydney\", \"country\": \"Australia\", \"zip\": \"2042\"}', 'Wendy Mackenzie | Sales Representative'),
('Perth Pasties | 48 pieces', 32.8000, '{\"company\": \"G\'day, Mate\", \"address\": \"170 Prince Edward Parade\\r\\nHunter\'s Hill\", \"city\": \"Sydney\", \"country\": \"Australia\", \"zip\": \"2042\"}', 'Wendy Mackenzie | Sales Representative'),
('Tourtire | 16 pies', 7.4500, '{\"company\": \"Ma Maison\", \"address\": \"2960 Rue St. Laurent\", \"city\": \"Montral\", \"country\": \"Canada\", \"zip\": \"H1J 1C3\"}', 'Jean-Guy Lauzon | Marketing Manager'),
('Pt chinois | 24 boxes x 2 pies', 24.0000, '{\"company\": \"Ma Maison\", \"address\": \"2960 Rue St. Laurent\", \"city\": \"Montral\", \"country\": \"Canada\", \"zip\": \"H1J 1C3\"}', 'Jean-Guy Lauzon | Marketing Manager'),
('Gnocchi di nonna Alice | 24 - 250 g pkgs.', 38.0000, '{\"company\": \"Pasta Buttini s.r.l.\", \"address\": \"Via dei Gelsomini, 153\", \"city\": \"Salerno\", \"country\": \"Italy\", \"zip\": \"84100\"}', 'Giovanni Giudici | Order Administrator'),
('Ravioli Angelo | 24 - 250 g pkgs.', 19.5000, '{\"company\": \"Pasta Buttini s.r.l.\", \"address\": \"Via dei Gelsomini, 153\", \"city\": \"Salerno\", \"country\": \"Italy\", \"zip\": \"84100\"}', 'Giovanni Giudici | Order Administrator'),
('Escargots de Bourgogne | 24 pieces', 13.2500, '{\"company\": \"Escargots Nouveaux\", \"address\": \"22, rue H. Voiron\", \"city\": \"Montceau\", \"country\": \"France\", \"zip\": \"71300\"}', 'Marie Delamare | Sales Manager'),
('Raclette Courdavault | 5 kg pkg.', 55.0000, '{\"company\": \"Gai pturage\", \"address\": \"Bat. B\\r\\n3, rue des Alpes\", \"city\": \"Annecy\", \"country\": \"France\", \"zip\": \"74000\"}', 'Eliane Noz | Sales Representative'),
('Camembert Pierrot | 15 - 300 g rounds', 34.0000, '{\"company\": \"Gai pturage\", \"address\": \"Bat. B\\r\\n3, rue des Alpes\", \"city\": \"Annecy\", \"country\": \"France\", \"zip\": \"74000\"}', 'Eliane Noz | Sales Representative'),
('Sirop d\'rable | 24 - 500 ml bottles', 28.5000, '{\"company\": \"Forts d\'rables\", \"address\": \"148 rue Chasseur\", \"city\": \"Ste-Hyacinthe\", \"country\": \"Canada\", \"zip\": \"J2S 7S8\"}', 'Chantal Goulet | Accounting Manager'),
('Tarte au sucre | 48 pies', 49.3000, '{\"company\": \"Forts d\'rables\", \"address\": \"148 rue Chasseur\", \"city\": \"Ste-Hyacinthe\", \"country\": \"Canada\", \"zip\": \"J2S 7S8\"}', 'Chantal Goulet | Accounting Manager');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
