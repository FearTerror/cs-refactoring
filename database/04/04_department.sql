-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Ноя 19 2023 г., 23:33
-- Версия сервера: 10.6.12-MariaDB-0ubuntu0.22.04.1
-- Версия PHP: 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `department`
--
CREATE DATABASE IF NOT EXISTS `department` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `department`;

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_head` (`department` VARCHAR(255), `head_first_name` VARCHAR(255), `head_last_name` VARCHAR(255), `head_gender` VARCHAR(1), `head_hire_date` DATE, `heads_from` DATE, `heads_to` DATE)   BEGIN
INSERT INTO heads (department, head, heads_from_to) VALUES (department, JSON_OBJECT("first_name", head_first_name, "last_name", head_last_name, "gender", head_gender, "hire_date", head_hire_date), CONCAT_WS('|', heads_from, heads_to));
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `heads`
--

CREATE TABLE `heads` (
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heads_from_to` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Дамп данных таблицы `heads`
--

INSERT INTO `heads` (`department`, `first name`,`last name`, `heads_from_to`) VALUES
('Customer Service', 'Tonny', 'Butterworth, \"gender\": \"F\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1988-10-17'),
('Customer Service', '{\"first_name\": \"Marjo\", \"last_name\": \"Giarratana\", \"gender\": \"F\", \"hire_date\": \"1988-02-12\"}', '1988-10-17|1992-09-08'),
('Customer Service', '{\"first_name\": \"Xiaobin\", \"last_name\": \"Spinelli\", \"gender\": \"F\", \"hire_date\": \"1991-08-17\"}', '1992-09-08|1996-01-03'),
('Customer Service', '{\"first_name\": \"Yuchang\", \"last_name\": \"Weedman\", \"gender\": \"M\", \"hire_date\": \"1989-07-10\"}', '1996-01-03|9999-01-01'),
('Development', '{\"first_name\": \"DeForest\", \"last_name\": \"Hagimont\", \"gender\": \"M\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1992-04-25'),
('Development', '{\"first_name\": \"Leon\", \"last_name\": \"DasSarma\", \"gender\": \"F\", \"hire_date\": \"1986-10-21\"}', '1992-04-25|9999-01-01'),
('Finance', '{\"first_name\": \"Ebru\", \"last_name\": \"Alpin\", \"gender\": \"M\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1989-12-17'),
('Finance', '{\"first_name\": \"Isamu\", \"last_name\": \"Legleitner\", \"gender\": \"F\", \"hire_date\": \"1985-01-14\"}', '1989-12-17|9999-01-01'),
('Human Resources', '{\"first_name\": \"Shirish\", \"last_name\": \"Ossenbruggen\", \"gender\": \"F\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1992-03-21'),
('Human Resources', '{\"first_name\": \"Karsten\", \"last_name\": \"Sigstam\", \"gender\": \"F\", \"hire_date\": \"1985-08-04\"}', '1992-03-21|9999-01-01'),
('Marketing', '{\"first_name\": \"Margareta\", \"last_name\": \"Markovitch\", \"gender\": \"M\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1991-10-01'),
('Marketing', '{\"first_name\": \"Vishwani\", \"last_name\": \"Minakawa\", \"gender\": \"M\", \"hire_date\": \"1986-04-12\"}', '1991-10-01|9999-01-01'),
('Production', '{\"first_name\": \"Krassimir\", \"last_name\": \"Wegerle\", \"gender\": \"F\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1988-09-09'),
('Production', '{\"first_name\": \"Rosine\", \"last_name\": \"Cools\", \"gender\": \"F\", \"hire_date\": \"1985-11-22\"}', '1988-09-09|1992-08-02'),
('Production', '{\"first_name\": \"Shem\", \"last_name\": \"Kieras\", \"gender\": \"M\", \"hire_date\": \"1988-10-14\"}', '1992-08-02|1996-08-30'),
('Production', '{\"first_name\": \"Oscar\", \"last_name\": \"Ghazalie\", \"gender\": \"M\", \"hire_date\": \"1992-02-05\"}', '1996-08-30|9999-01-01'),
('Quality Management', '{\"first_name\": \"Peternela\", \"last_name\": \"Onuegbe\", \"gender\": \"F\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1989-05-06'),
('Quality Management', '{\"first_name\": \"Rutger\", \"last_name\": \"Hofmeyr\", \"gender\": \"F\", \"hire_date\": \"1989-01-07\"}', '1989-05-06|1991-09-12'),
('Quality Management', '{\"first_name\": \"Sanjoy\", \"last_name\": \"Quadeer\", \"gender\": \"F\", \"hire_date\": \"1986-08-12\"}', '1991-09-12|1994-06-28'),
('Quality Management', '{\"first_name\": \"Dung\", \"last_name\": \"Pesch\", \"gender\": \"M\", \"hire_date\": \"1989-06-09\"}', '1994-06-28|9999-01-01'),
('Research', '{\"first_name\": \"Arie\", \"last_name\": \"Staelin\", \"gender\": \"M\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1991-04-08'),
('Research', '{\"first_name\": \"Hilary\", \"last_name\": \"Kambil\", \"gender\": \"F\", \"hire_date\": \"1988-01-31\"}', '1991-04-08|9999-01-01'),
('Sales', '{\"first_name\": \"Przemyslawa\", \"last_name\": \"Kaelbling\", \"gender\": \"M\", \"hire_date\": \"1985-01-01\"}', '1985-01-01|1991-03-07'),
('Sales', '{\"first_name\": \"Hauke\", \"last_name\": \"Zhang\", \"gender\": \"M\", \"hire_date\": \"1986-12-30\"}', '1991-03-07|9999-01-01');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
