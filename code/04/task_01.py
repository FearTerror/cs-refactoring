import sys

"""
Обчислити об’єм чотирикутної піраміди, якщо задано її висота та довжина сторони основи.
"""

class PyramidCalc:
    def __init__(self, height, base_length):
        self.height = height
        self.base_length = base_length

    def calc_volume(self):
        if self.validate_input():
            volume = (self.height * self.base_length ** 2) / 3
            return volume
        else:
            sys.exit(1)

    def validate_input(self):
        if self.height <= 0:
            print(f"Довжина висоти ({self.height}) не може бути менше або дорівнювати 0.")
            return False
        if self.base_length <= 0:
            print(f"Довжина сторони ({self.base_length}) не може бути менше або дорівнювати 0.")
            return False
        return True
def inputtest():
    try:
        height = float(input('Введіть довжину висоти піраміди:'))
        base_length = float(input('Введіть довжину сторони піраміди:'))
        return height, base_length
    except ValueError:
        print('Введене значення не є числом.')
        sys.exit(1)
def end_prog(volume):
    print(f"Об’єм піраміди дорівнює {volume}.")
def main():
    height, base_length = inputtest()
    calc = PyramidCalc(height, base_length)
    volume = calc.calc_volume()
    end_prog(volume)
    sys.exit(0)



if __name__ == '__main__':
    main()
