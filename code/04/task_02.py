import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (2 * (sin(2x))^2) / ((cos(x + 2*y))^2)
Z2 = 2 * Z1 / sin(5- 8*x)
Z3 = (Z2 - Z1) / (x + y)
'''
class ZCalc:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def calc_z1(self):
        sin_2x = math.sin(2 * self.x)
        cos_x_2y = math.cos(self.x + 2 * self.y)
        return (2 * sin_2x ** 2) / cos_x_2y ** 2

    def calc_z2(self, z1):
        sin_8x = math.sin(5 - 8 * self.x)
        return 2 * z1 / sin_8x

    def calc_z3(self, z1):
        sin_8x = math.sin(5 - 8 * self.x)
        denominator = self.x + self.y
        return (sin_8x - z1) / denominator if denominator != 0 else None
def inputtest():
    try:
        x = float(input('Введіть значення x: '))
        y = float(input('Введіть значення y: '))
        return x, y
    except ValueError:
        print('Некоректні дані. Введіть числові значення для x та y.')
        sys.exit(1)

def end_prog(z1,z2,z3):
    if z3 is not None:
        print(f"Z1 = {z1}")
        print(f"Z2 = {z2}")
        print(f"Z3 = {z3}")
        sys.exit(0)

    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
    sys.exit(1)
def main():
    x,y = inputtest()
    calc = ZCalc(x, y)
    z1 = calc.calc_z1()
    if z1 != 0:
        z2 = calc.calc_z2(z1)
        z3 = calc.calc_z3(z1)
        end_prog(z1,z2,z3)
    else:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
if __name__ == "__main__":
    main()
