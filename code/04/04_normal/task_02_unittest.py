import unittest
import sys
from unittest.mock import patch
from task_02 import main
from io import StringIO
class MyTestCase(unittest.TestCase):
    @patch('sys.stdin', StringIO('dummy\n9\n'))
    @patch('sys.stdout',new_callable=StringIO)
    def test_valid_input(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),"Введіть значення x: Некоректні дані. Введіть числові значення для x та y.\n")
    @patch('sys.stdin', StringIO('9\ndummy\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_valid_input_sec(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть значення x: Введіть значення y: Некоректні дані. Введіть числові значення для x та y.\n")


    @patch('sys.stdin', StringIO('0\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),
                         "Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")
    @patch('sys.stdin', StringIO('-1\n1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),
                         "Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('1\n-1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_2(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),
                         "Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n")

    @patch('sys.stdin', StringIO('9\n9\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_success(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(),
                         "Введіть значення x: Введіть значення y: Z1 = 13.216504600678801\nZ2 = 30.897009831397604\nZ3 = -0.6867213678724156\n")


if __name__ == '__main__':    unittest.main()