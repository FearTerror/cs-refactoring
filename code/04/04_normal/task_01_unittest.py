import unittest
import sys
from unittest.mock import patch
from task_01 import main
from io import StringIO

class MyTestCase(unittest.TestCase):
    @patch('sys.stdin', StringIO('dummy\n9\n'))
    @patch('sys.stdout',new_callable=StringIO)
    def test_valid_input(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),"Введіть довжину висоти піраміди:Введене значення не є числом.\n")
    @patch('sys.stdin', StringIO('9\ndummy\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_valid_input_2(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Введене значення не є числом.\n")

    @patch('sys.stdin', StringIO('-1\n-1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Довжина висоти (-1.0) не може бути менше або дорівнювати 0.\n")
    @patch('sys.stdin', StringIO('1\n-1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_2(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),
                         "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Довжина сторони (-1.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('0\n1\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_1(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Довжина висоти (0.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('1\n0\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_2(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),
                         "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Довжина сторони (0.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('9\n9\n'))
    @patch('sys.stdout', new_callable=StringIO)
    def test_success(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(),
                         "Введіть довжину висоти піраміди:Введіть довжину сторони піраміди:Об’єм піраміди дорівнює 243.0.\n")


if __name__ == '__main__':
    unittest.main()